const express = require('express');
const http = require('http');
const debug = require('debug')('server:*')

const app = express();
const server = http.Server(app);

app.set('view engine', 'ejs');
app.set('views', './src/views');

app.get('/', (req, res) => {
    res.render(
      'home',
      {
        homeTitle: 'Web Server Demo'
      }
    );
  });

const port = process.env.PORT || 8069;
server.listen(port, () => {
    debug(`Server listening on port ${port}`);
});